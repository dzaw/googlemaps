var googleMap = {
    map : null,
    parking1lat: 50.08877509,
    parking1lng: 19.98764367,
    parking2lat: 50.08776653,
    parking2lng: 19.98432671,
    mainentrlat: 50.087421,
    mainentrlng: 19.984904,
    trafficLayer: null,
    transitLayer: null,
    bikeLayer: null,
    directionsDisplay: [],
    directionsService: null,
    altRoute: [],
    polyArray: [],
    routeInfo: [],
    userLocation: [],
    geocoder: null,
    value: [],
    markers: [],
    markerNavArray: [],
    initMap : function() {
        googleMap.map = new google.maps.Map(document.getElementById('how-to-get-map'), {
          center: {lat: 50.088329, lng: 19.985689},
          zoom: 16,
          styles: [{
            stylers: [{
              saturation: -100
            }]
          }]
        });
        
        // traffic
        googleMap.trafficLayer = new google.maps.TrafficLayer();
        googleMap.trafficLayer.setMap(googleMap.map);
        // transit
        googleMap.transitLayer = new google.maps.TransitLayer();
        googleMap.transitLayer.setMap(googleMap.map);
        // bike
        //googleMap.bikeLayer = new google.maps.BicyclingLayer();
        //googleMap.bikeLayer.setMap(googleMap.map);
        
        // shop polygon
        var polygonCoords = [ {lat: 50.0886851, lng: 19.9842630},{lat: 50.0888503, lng: 19.9858831},{lat: 50.0897452, lng: 19.9856367},{lat: 50.0893115, lng: 19.9869667},{lat: 50.0889398, lng: 19.9870954},{lat: 50.0888021, lng: 19.9871491},{lat: 50.0886645, lng: 19.9871169},{lat: 50.0883272, lng: 19.9870954},{lat: 50.0881138, lng: 19.9869667},{lat: 50.0875700, lng: 19.9866233},{lat: 50.0872464, lng: 19.9864517},{lat: 50.0871919, lng: 19.9864260},{lat: 50.0871305, lng: 19.9863516},{lat: 50.0871248, lng: 19.9862827},{lat: 50.0871282, lng: 19.9862069},{lat: 50.0871365, lng: 19.9861125},{lat: 50.0871682, lng: 19.9859782},{lat: 50.0875074, lng: 19.9845863},{lat: 50.0883155, lng: 19.9843826},{lat: 50.0886645, lng: 19.9842630}];
        var polygon = new google.maps.Polygon({ paths: polygonCoords, strokeColor: '#FF0000', strokeOpacity: 0.8, strokeWeight: 2, fillColor: '#FF0000', fillOpacity: 0.35 });
        polygon.setMap(googleMap.map);
        var polygonInfo = new google.maps.InfoWindow({size: new google.maps.Size(150, 50)});
        polygonInfo.setContent("<b>CH Serenada</b>");
        polygonInfo.setPosition( new google.maps.LatLng(50.088329, 19.985689) );
        polygonInfo.open(googleMap.map);
        google.maps.event.addListener(polygon, 'click', function(event) {
            polygonInfo.open(googleMap.map);
        });
        google.maps.event.addListener(googleMap.map, 'click', function(event) {
            polygonInfo.close();
        });
        google.maps.Polyline.prototype.getBounds = function() {
            var bounds = new google.maps.LatLngBounds();
            this.getPath().forEach(function(item, index) {
                bounds.extend(new google.maps.LatLng(item.lat(), item.lng()));
            });
            return bounds;
        };
        
        googleMap.stepDisplay = new google.maps.InfoWindow();
        googleMap.stepDisplay.setOptions({maxWidth:250});
    },
    addMarker : function(id, lat, lng, title, description) {
        var latLng = {lat: lat, lng: lng};
        var marker = new google.maps.Marker({
            id: id,
            position: latLng,
            map: googleMap.map,
            title: title,
            description: description
        });
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(marker.description);
            infowindow.open(googleMap.map, this);
        });
        
        if (googleMap.getMarker(title) != undefined){
            var index = googleMap.markers.indexOf(googleMap.getMarker(title));
            if (index > -1) {
                googleMap.markers[index].setMap(null);
                googleMap.markers.splice(index, 1);
            }
        }
        googleMap.markers.push(marker);
    },
    getMarker : function(title){
        var result = googleMap.markers.filter(function( obj ) {
            if (title){
                if (obj.title == title){
                    return obj;
                };
            }
        }); 
        return result[0];
    },
    getUserLocation : function(){
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                googleMap.userLocation = [position.coords.latitude, position.coords.longitude];
                //console.log(googleMap.userLocation);
            }, function() {
                alert('Error: The Geolocation service failed. Insecure http connection or location denied.');
            });
        } else {
            alert('Error: Your browser doesn\'t support geolocation.');
        }
    },
    getUserLocationContinuous : function(extra){
        googleMap.watch = null;
        function updatePosition() {
            var options = {enableHighAccuracy: true,timeout: 5000,maximumAge: 0,desiredAccuracy: 0, frequency: 100 };
            googleMap.watch = window.navigator.geolocation.watchPosition(onSuccess, onError, options);
        }
        function onSuccess(position) {     
            var info = 'Position updated: lat: ' + position.coords.latitude + ', lng:' + position.coords.longitude + ', timestamp: ' + position.timestamp;
            //console.log(info);
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            googleMap.userLocation = [position.coords.latitude, position.coords.longitude];
            googleMap.addMarker(0, googleMap.userLocation[0], googleMap.userLocation[1], 'user', 'Twoja lokalizacja');
            googleMap.getMarker("user").setIcon({ path: google.maps.SymbolPath.CIRCLE, scale: 8, fillColor: "#00408F", fillOpacity: 0.9, strokeColor: "#0072FF", strokeWeight: 32, strokeOpacity: 0.4
            });
        }
        function onError(error) {
          console.log('Error code: ' + error.code + ', ' + 'message: ' + error.message);
        }
        updatePosition();
    },
    showUserLocation(){
        if ( googleMap.userLocation.length > 0) {
            googleMap.addMarker(0, googleMap.userLocation[0], googleMap.userLocation[1], 'user', 'Twoja lokalizacja');
            var pos = {
                lat: googleMap.userLocation[0],
                lng: googleMap.userLocation[1]
            };
            googleMap.map.setCenter(pos);
            googleMap.map.setZoom(16);
            //var infowindow = new google.maps.InfoWindow();
            //infowindow.setContent(googleMap.getMarker("user").description);
            //infowindow.open(googleMap.map, googleMap.getMarker("user"));
            
            googleMap.getMarker("user").setIcon({
                path: google.maps.SymbolPath.CIRCLE,
                scale: 8,
                fillColor: "#00408F",
                fillOpacity: 0.9,
                strokeColor: "#0072FF",
                strokeWeight: 32,
                strokeOpacity: 0.4
            });
        }
    },
    setValue : function(name, value){
        googleMap.value[name] = value;
    },
    calculateRoute : function(directionsService, directionsDisplay, startPoint, endPoint, travelMode, id) {
            directionsService.route({
              origin: startPoint,
              destination: endPoint,
              travelMode: travelMode,
              drivingOptions: {
                departureTime: new Date(),
                trafficModel: google.maps.TrafficModel.BEST_GUESS
              },
              provideRouteAlternatives: true
            }, function(response, status) {
              if (status === 'OK') {
                  for (var j = 0; j < response.routes.length; j++) {
                      googleMap.setValue(id, response);
                  }
              } else {
                window.alert('Directions request failed due to ' + status);
              }
            });
    },
    getDirections : function(startLat, startLng, startAddress, travelMode) {
        googleMap.directionsService = new google.maps.DirectionsService;
        
        if (startAddress != null) {
            var start = startAddress;
        }
        if ( startLat != null && startLng != null ) {
            var start = new google.maps.LatLng(startLat, startLng); 
        }
        
        var end1 =  new google.maps.LatLng(googleMap.parking1lat, googleMap.parking1lng);
        var end2 =  new google.maps.LatLng(googleMap.parking2lat, googleMap.parking2lng);
        var end3 =  new google.maps.LatLng(googleMap.mainentrlat, googleMap.mainentrlng);
        
        var lineSymbol = {
            path: google.maps.SymbolPath.CIRCLE,
            fillOpacity: 1,
            scale: 4
        };
        var polylineDottedGrey = {
            map: googleMap.map,
            strokeColor: "grey",
            strokeOpacity: 0,
            fillOpacity: 0,
            icons: [{
                icon: lineSymbol,
                offset: '0',
                repeat: '10px'
            }],
            zIndex: 200
        };
        var polylineDottedBlue = {
            map: googleMap.map,
            strokeColor: "grey",
            strokeOpacity: 0,
            fillOpacity: 0,
            icons: [{
                icon: lineSymbol,
                offset: '0',
                repeat: '10px'
            }],
            zIndex: 200
        };
        var polylineSolidGrey = {
            map: googleMap.map,
            strokeColor: "grey",
            strokeOpacity: 0.6,
            strokeWeight: 7,
            zIndex: 200
        };
        var polylineSolidBlue = {
            map: googleMap.map,
            strokeColor: "grey",
            strokeOpacity: 0.5,
            strokeWeight: 7,
            zIndex: 200
        };
        
        if (travelMode == 'DRIVING') {
            googleMap.calculateRoute(googleMap.directionsService, googleMap.directionsDisplay[0], start, end1, travelMode, 'dist1');
            googleMap.calculateRoute(googleMap.directionsService, googleMap.directionsDisplay[0], start, end2, travelMode, 'dist2');
            //if route depends on chosen parking:
            /*
            var end = $("#parking-entr").val();
            if ( end == "end1" ) {
                googleMap.calculateRoute(googleMap.directionsService, googleMap.directionsDisplay[0], start, end1, travelMode, 'dist1');
            } 
            else if ( end == "end2" ) {
                googleMap.calculateRoute(googleMap.directionsService, googleMap.directionsDisplay[0], start, end2, travelMode, 'dist1'); 
            }
            else {
                googleMap.calculateRoute(googleMap.directionsService, googleMap.directionsDisplay[0], start, end1, travelMode, 'dist1');
            }
            */
        }
        else if (travelMode == 'WALKING') {
            googleMap.calculateRoute(googleMap.directionsService, googleMap.directionsDisplay[0], start, end3, travelMode, 'dist1');
        }
        
        for (var i=0; i<3;i++){
            googleMap.directionsDisplay[i] = new google.maps.DirectionsRenderer({
                suppressMarkers: true
                //,suppressPolylines: true
            });
            googleMap.directionsDisplay[i].setMap(googleMap.map);
            googleMap.directionsDisplay[i].setOptions({
                polylineOptions: {
                    strokeColor: "white",
                    strokeOpacity: 0
                }
            });
        }
        
        setTimeout(function() {
            googleMap.polyArray.forEach(function(polyline, index) {
                polyline.setMap(null);
            });
            googleMap.polyArray = [];
            googleMap.routeInfo.forEach(function(routeInfo, index) {
                routeInfo.close();
            });
            googleMap.routeInfo = [];
            
            if (travelMode == "DRIVING") {
                if (googleMap.value['dist1'].routes[0].legs[0].distance.value <= googleMap.value['dist2'].routes[0].legs[0].distance.value){ var closestRoute = 'dist1'; } else { var closestRoute = 'dist2'; }
                //if route depends on chosen parking:
                //var closestRoute = 'dist1';
            } else if (travelMode == "WALKING") {
                var closestRoute = 'dist1';
            }
            
            for (var j = 0; j < googleMap.value[closestRoute].routes.length; j++) {
                googleMap.path = new google.maps.MVCArray();
                if (travelMode == "WALKING") {
                    var polyOptions = polylineDottedGrey;
                }
                else {
                    var polyOptions = polylineSolidGrey;
                }
                googleMap.polyArray.push(new google.maps.Polyline(polyOptions));
                if (j == 0) googleMap.polyArray[0].setOptions({
                    strokeColor: '#0093FF',
                    zIndex: 300
                });
                googleMap.polyArray[googleMap.polyArray.length - 1].setPath(googleMap.path);
                
                google.maps.event.addListener(googleMap.polyArray[googleMap.polyArray.length - 1], 'click', function(e) {
                    for (var i = 0; i < googleMap.polyArray.length; i++) {
                        googleMap.polyArray[i].setOptions(polyOptions);
                    }
                    var clickedIndex = googleMap.polyArray.indexOf(this);
                    googleMap.clickedIndex = clickedIndex;
                    googleMap.getDirectionsSteps(googleMap.altRoute[clickedIndex]);
                    this.setOptions({
                        strokeColor: "#0093FF",
                        zIndex: 300
                    });
                    
                    if (travelMode == "WALKING") {
                        var routeInfoContent = "<img src='Lib/navi-google-car.png' style='height:25px;'><b>" + googleMap.altRoute[clickedIndex].duration.text + "</b><br>" + googleMap.altRoute[clickedIndex].distance.text;
                    }
                    else {
                        if (googleMap.altRoute[clickedIndex].duration.value > googleMap.altRoute[clickedIndex].duration_in_traffic.value) {
                        var textcolor = "green";
                        }
                        else if (googleMap.altRoute[clickedIndex].duration_in_traffic.value <= (googleMap.altRoute[clickedIndex].duration.value * 1.1) ) {
                            var textcolor = "orange";
                        }
                        else {
                            var textcolor = "red";
                        }
                        var routeInfoContent = "<img src='Lib/navi-google-car.png' style='height:25px;'><b><span style='color:" + textcolor + ";'>" + googleMap.altRoute[clickedIndex].duration_in_traffic.text + "</span></b><br>" + googleMap.altRoute[clickedIndex].distance.text;
                    }
                    
                    googleMap.routeInfo[clickedIndex].close();
                    googleMap.routeInfo[clickedIndex] = new google.maps.InfoWindow();
                    googleMap.routeInfo[clickedIndex].setContent(routeInfoContent);
                    googleMap.routeInfo[clickedIndex].setPosition( e.latLng );
                    googleMap.routeInfo[clickedIndex].open(googleMap.map);
                    
                });
                for (var i = 0, len = googleMap.value[closestRoute].routes[j].overview_path.length; i < len; i++) {
                    googleMap.path.push(googleMap.value[closestRoute].routes[j].overview_path[i]);
                }
                googleMap.altRoute[j] = googleMap.value[closestRoute].routes[j].legs[0];
                googleMap.getDirectionsSteps(googleMap.altRoute[0]);
                googleMap.map.fitBounds(googleMap.polyArray[0].getBounds());
                
                googleMap.routeInfo[j] = new google.maps.InfoWindow();
                if (travelMode == "WALKING") {
                    var routeInfoContent = "<img src='Lib/navi-google-man.png' style='height:25px;'><b>" + googleMap.altRoute[j].duration.text + "</b><br>" + googleMap.altRoute[j].distance.text;
                }
                else {
                    if (googleMap.altRoute[j].duration.value > googleMap.altRoute[j].duration_in_traffic.value) {
                        var textcolor = "green";
                        }
                        else if (googleMap.altRoute[j].duration_in_traffic.value <= (googleMap.altRoute[j].duration.value * 1.1) ) {
                            var textcolor = "orange";
                        }
                        else {
                            var textcolor = "red";
                        }
                    var routeInfoContent = "<img src='Lib/navi-google-car.png' style='height:25px;'><b><span style='color:" + textcolor + ";'>" + googleMap.altRoute[j].duration_in_traffic.text + "</span></b><br>" + googleMap.altRoute[j].distance.text;
                }

                googleMap.routeInfo[j].setContent(routeInfoContent);
                var pos = googleMap.polyArray[j].latLngs.b[0].b[googleMap.polyArray[j].latLngs.b[0].b.length / 2 | 0];
                googleMap.routeInfo[j].setPosition( pos );
                googleMap.routeInfo[j].open(googleMap.map);

            }
            
        }, 2500);
        
    },
    navigateUser : function(action){
        //console.log('Navigate', action);
        googleMap.watch = null;
        function updatePosition() {
            var options = {enableHighAccuracy: true,timeout: 5000,maximumAge: 0,desiredAccuracy: 0, frequency: 100 };
            googleMap.watch = window.navigator.geolocation.watchPosition(onSuccess, onError, options);
        }
        function onSuccess(position) {     
            var info = 'Position updated: lat: ' + position.coords.latitude + ', lng:' + position.coords.longitude + ', timestamp: ' + position.timestamp;
            //console.log(info);
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            googleMap.userLocation = [position.coords.latitude, position.coords.longitude];
            googleMap.showUserLocation();
            
            googleMap.markerNavArray.forEach(function(navMarker, index) {
                var distance = googleMap.getDistance(googleMap.getMarker("user").position, navMarker.position);
                if (distance < 30) {
                    var text = googleMap.currentRoute.steps[index].instructions;
                    googleMap.stepDisplay.setContent(text);
                    googleMap.stepDisplay.open(googleMap.map, navMarker);
                }
            });
        }
        function clearWatch() {
            window.navigator.geolocation.clearWatch(googleMap.watch);
            googleMap.watch = null;
        }
        function onError(error) {
          //console.log('Error code: ' + error.code + ', ' + 'message: ' + error.message);
        }
        if (action == "start"){
            updatePosition();
        }
        if (action == "stop"){
            clearWatch();
        }
    },
    getDirectionsSteps : function(route) {
        googleMap.currentRoute = route;
        if(googleMap.markerNavArray.length>0){
            googleMap.markerNavArray.forEach(function(marker) {
                marker.setMap(null);
            });
            googleMap.markerNavArray = [];
        }
        for (var i = 0; i < googleMap.currentRoute.steps.length; i++) {
            var marker = new google.maps.Marker({
                position: googleMap.currentRoute.steps[i].start_point,
                map: googleMap.map
            });
            marker.setIcon({
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5,
                fillColor: "#FFFFFF",
                fillOpacity: 1,
                strokeColor: "#0069EC",
                strokeWeight: 4,
                strokeOpacity: 0.8
            });
            attachInstructionText(marker, googleMap.currentRoute.steps[i].instructions);
            googleMap.markerNavArray[i] = marker;
            
            //add ending point
            if (i == (googleMap.currentRoute.steps.length-1) ){
                var lastmarker = new google.maps.Marker({
                    position: googleMap.currentRoute.steps[i].end_point,
                    map: googleMap.map
                });
                googleMap.markerNavArray.push(lastmarker);
                
                var dist1 = googleMap.getDistance(googleMap.getMarker("wjazd1").position, lastmarker.position);
                if (dist1 < 10) {
                    attachInstructionText(lastmarker, "Skręć <b>w prawo</b> do wjazdu na parking. Dotarłeś do celu <b>CH Serenada</b>");
                }
                var dist2 = googleMap.getDistance(googleMap.getMarker("wjazd2").position, lastmarker.position);
                if (dist2 < 10) {
                    attachInstructionText(lastmarker, "Skręć <b>w lewo</b> do wjazdu na parking. Dotarłeś do celu <b>CH Serenada</b>");
                }
            }
            
        }
        
        //start & end pins
        googleMap.markerNavArray[0].setIcon({
            url:'Lib/navi-pin-A.png',
            scaledSize: new google.maps.Size(35, 50)
        });
        googleMap.markerNavArray[googleMap.markerNavArray.length-1].setIcon({
            url:'Lib/navi-pin-B.png',
            scaledSize: new google.maps.Size(35, 50)
        });
        
        // attach info to pins on route
        function attachInstructionText(marker, text) {
          google.maps.event.addListener(marker, 'click', function() {
              if (googleMap.clickedIndex){
                  googleMap.routeInfo[googleMap.clickedIndex].close();
              }
              googleMap.stepDisplay.setContent(text);
              googleMap.stepDisplay.open(googleMap.map, marker);
          });
        }
    },
    getDistance : function(source, destination){
        return google.maps.geometry.spherical.computeDistanceBetween(
            new google.maps.LatLng(source.lat(), source.lng()),
            new google.maps.LatLng(destination.lat(), destination.lng())
        );
    },
    getDevice : function(){
        var device;
        if (window.matchMedia('(max-width: 767px)').matches){
            device = "mobile";
        }
        else {
            device = "desktop";
        }
        return device;
    },
    geocodeAddress : function(address) { //geocoder api not enabled in google dev consoled
        googleMap.geocoder.geocode({ 'address' : address }, function( results, status ) {
            if( status == google.maps.GeocoderStatus.OK ) {
                //console.log( results[0].geometry.location.lat() );
                googleMap.map.setCenter( results[0].geometry.location );
                var marker = new google.maps.Marker( {
                    map     : map,
                    position: results[0].geometry.location
                } );
            } else {
                alert( 'Geocode was not successful for the following reason: ' + status );
            }
        });
    },
    overrideZoomBtn : function(){
        $('#zoomControl').find('.zoom-plus').click(function(){
            googleMap.map.setZoom(googleMap.map.getZoom() + 1);
            return false;
        });
        $('#zoomControl').find('.zoom-minus').click(function(){
            googleMap.map.setZoom(googleMap.map.getZoom() - 1);
            return false;
        });
    },
    getUserLocation2 :  function() {
        if (googleMap.userLocation.length <= 0){
            googleMap.getUserLocation();
        }
		googleMap.showUserLocation();		
    },
    getDirections2 : function() {	
        if (googleMap.userLocation.length <= 0){
            googleMap.getUserLocation();
        }
        if (googleMap.form.type == null) {
            googleMap.form.type = 'DRIVING'; //travelModes: 'DRIVING', 'BICYCLING', 'TRANSIT', 'WALKING'
        }
        if (googleMap.directionsDisplay.length = 0) {
            googleMap.directionsDisplay[0].setMap(null);
            googleMap.directionsDisplay[0].setDirections({routes: []});
        }
        googleMap.getDirections(googleMap.userLocation[0], googleMap.userLocation[1], null, googleMap.form.type);
    },
    getDirectionsInputBox : function() {
        if ($("#userAddress").val() == "") {
            alert("Do wyznaczenia trasy - wpisz adres lub kliknij 'moja lokalizacja'.");
        }
        else if ($("#userAddress").val() == "Moja lokalizacja") {
            googleMap.getDirections2();	
            $(".navi-contents-left").toggle();
        }
        // symulacja adresu:
        else if ($("#userAddress").val() == "Wrocławska 100, Kraków") {
            googleMap.userLocation = [50.080840, 19.917620];
            googleMap.showUserLocation();
            googleMap.getDirections2();	
            //$(".navi-contents-left").toggle();
        }
        else if ($("#userAddress").val() == "Dietla 20, Kraków") {
            googleMap.userLocation = [50.050732, 19.939402];
            googleMap.showUserLocation();
            googleMap.getDirections2();	
        }
        else if ($("#userAddress").val() == "Jana Pawła II 300, Kraków") {
            googleMap.userLocation = [50.071803, 20.034938];
            googleMap.showUserLocation();
            googleMap.getDirections2();	
        }
        //
        else {
            googleMap.getDirectionsFromAddress();
            $(".navi-contents-left").toggle();
        }
    },
    getDirectionsFromAddress : function() {
        //console.log($scope.userAddress);
        if (googleMap.form.type == null) {
            googleMap.form.type = 'DRIVING';
        }
        if (googleMap.directionsDisplay.length = 0) {
            googleMap.directionsDisplay[0].setMap(null);
            googleMap.directionsDisplay[0].setDirections({routes: []});
        }
        googleMap.getDirections(null, null, googleMap.userAddress, googleMap.form.type);
    },
    checkParking : function() {
        var i = parseInt(googleMap.parkinglevel);
        switch(googleMap.parkingsector) {
            case 's1':
                var n = googleMap.parkingData[i].TotalNormal - googleMap.parkingData[i].OccupyNormal;
                googleMap.freespaces = n + " wolnych miejsc";
                break;
            case 's2':
                var n = googleMap.parkingData[i].TotalDisabled - googleMap.parkingData[i].OccupyDisabled;
                googleMap.freespaces = n + " wolnych miejsc";
                break;
            case 's3':
                var n = googleMap.parkingData[i].TotalMamas - googleMap.parkingData[i].OccupyMamas;
                googleMap.freespaces = n + " wolnych miejsc";
                break;
        }
    }
};